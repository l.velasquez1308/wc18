import { Component, OnInit, NgZone } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import firebaseWebApi = require("nativescript-plugin-firebase/app");
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Observable } from "rxjs";
import { ListView } from "ui/list-view";
import { Page } from "ui/page";
@Component({
  selector: "Search",
  moduleId: module.id,
  styleUrls: ["./search.component.css"],
  templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
  games: ObservableArray<any> = new ObservableArray<any>([]);
  loading: boolean = false;
  busy: boolean = true;

  constructor(public page: Page, private ngZone: NgZone) {
    // Use the component constructor to inject providers.
    let self = this;
    const onValueEvent = snap => {
      this.ngZone.run(() => {
        if (snap.error) {
          console.log("Listener error: " + snap.error);
        } else {
          self.games = snap.val();
          self.loading = true;
          self.busy = false;
          // console.log(JSON.stringify(self.games));
        }
      });
    };

    firebaseWebApi
      .database()
      .ref("/games")
      .on("value", onValueEvent);
  }

  ngOnInit(): void {}

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }
}
