import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ApiService } from "~/shared/api-service.service";
import { Router, NavigationExtras } from "@angular/router";
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import * as LocalNotifications from "nativescript-local-notifications";

@Component({
  selector: "Groups",
  moduleId: module.id,
  templateUrl: "./groups.component.html",
  styleUrls: ["./groups.component.css"],
  providers: [ApiService]
})
export class GroupsComponent implements OnInit {
  groups: Array<any>;
  busy: boolean;
  loaded: boolean = false;

  constructor(
    private apiService: ApiService,
    private routerExtensions: RouterExtensions,
    private router: Router
  ) {
    this.busy = true;
    // Use the component constructor to inject providers.
    this.groups = [];
    // Init your component properties here.
    setTimeout(() => {
      this.getGroups();
    }, 190);
  }

  ngOnInit(): void {
    // LocalNotifications.schedule([
    //   {
    //     id: 1,
    //     title: "Sound & Badge",
    //     body: "Who needs a push service anyway?",
    //     badge: 1,
    //     at: new Date(new Date().getTime() + 5 * 1000) // 5 seconds from now
    //   }
    // ]);
    // // adding a handler, so we can do something with the received notification.. in this case an alert
    // LocalNotifications.addOnMessageReceivedCallback(data => {
    //   alert({
    //     title: "Local Notification received",
    //     message: `id: '${data.id}', title: '${data.title}'.`,
    //     okButtonText: "Roger that"
    //   });
    // });
  }

  getGroups() {
    this.apiService.getGames().subscribe((res: any) => {
      this.busy = false;
      this.loaded = true;
      this.groups = res.standings;
    });
  }

  navigateToGroup(ind) {
    this.routerExtensions.navigate(["/group/"], {
      queryParams: {
        group: JSON.stringify(this.groups[ind])
      },
      // clearHistory: true,
      transition: {
        name: "slideLeft",
        duration: 400
        //   // curve: "linear"
      }
    });
  }

  goBack() {
    this.routerExtensions.back();
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }
}
