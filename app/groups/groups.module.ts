import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { GroupsRoutingModule } from "./groups-routing.module";
import { GroupsComponent } from "./groups.component";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    GroupsRoutingModule,
    NativeScriptUIListViewModule
  ],
  declarations: [GroupsComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class GroupsModule {}
