import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import { switchMap } from "rxjs/operators";
import { ApiService } from "~/shared/api-service.service";
import {
  TabView,
  SelectedIndexChangedEventData,
  TabViewItem
} from "ui/tab-view";

@Component({
  moduleId: module.id,
  selector: "app-group-details",
  templateUrl: "./group-details.component.html",
  styleUrls: ["./group-details.component.css"],
  providers: [ApiService]
})
export class GroupDetailsComponent implements OnInit {
  group: Array<any>;
  selectedIndex: number;

  constructor(
    private apiService: ApiService,
    private pageRoute: PageRoute,
    private routerExtensions: RouterExtensions,
    private route: ActivatedRoute
  ) {
    this.group = [];
    this.route.queryParams.subscribe(params => {
      this.group = JSON.parse(params["group"]);
    });
  }

  ngOnInit() {
    this.selectedIndex = 2;
    // this.getGroups(this.id);
  }

  getGroups(id) {
    this.apiService.getGames().subscribe((res: any) => {
      this.group = res.standings[id];
    });
  }

  getPlayers(tabInd) {
    this.apiService
      .getPlayers(this.group[tabInd].teamId)
      .subscribe((res: any) => {
        this.group[tabInd].players = res.players;
      });
  }

  onIndexChanged(args) {
    let tabView = <TabView>args.object;
    this.getPlayers(tabView.selectedIndex);
  }

  goBack() {
    this.routerExtensions.back();
  }
}
