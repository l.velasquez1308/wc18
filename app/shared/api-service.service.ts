import { Injectable } from "@angular/core";
import { Observable as RxObservable } from "rxjs";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import config from "~/shared/config";

@Injectable()
export class ApiService {
  constructor(private http: HttpClient) {}

  getGames() {
    let headers = this.createRequestHeader();
    return this.http.get(config.url + `competitions/467/leagueTable`, {
      headers: headers
    });
  }

  getPlayers(id) {
    let headers = this.createRequestHeader();
    return this.http.get(config.url + `teams/${id}/players`, {
      headers: headers
    });
  }

  getFixtures() {
    let headers = this.createRequestHeader();
    return this.http.get(config.url + `competitions/467/fixtures`, {
      headers
    });
  }

  private createRequestHeader() {
    // set headers here e.g.
    let headers = new HttpHeaders({
      // "user-key": api.userKey,
      "X-Auth-Token": config.key
      // Authorization: "Bearer " + BackendService.token
    });

    return headers;
  }
}
