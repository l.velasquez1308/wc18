import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import firebaseWebApi = require("nativescript-plugin-firebase/app");

@Injectable()
export class FirebaseService {
  constructor(private http: HttpClient) {}

  getGames() {
    firebaseWebApi
      .database()
      .ref("/games")
      .once("value")
      .then(result => console.log(JSON.stringify(result)))
      .catch(error => console.log("Error: " + error));
  }
}
