import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { GroupDetailsComponent } from "~/groups/group-details/group-details.component";

const routes: Routes = [
  { path: "", redirectTo: "/groups", pathMatch: "full" },
  { path: "groups", loadChildren: "./groups/groups.module#GroupsModule" },
  { path: "browse", loadChildren: "./fixtures/fixture.module#FixtureModule" },
  { path: "search", loadChildren: "./search/search.module#SearchModule" },
  {
    path: "featured",
    loadChildren: "./featured/featured.module#FeaturedModule"
  },
  {
    path: "settings",
    loadChildren: "./settings/settings.module#SettingsModule"
  },
  {
    path: "group",
    component: GroupDetailsComponent
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
