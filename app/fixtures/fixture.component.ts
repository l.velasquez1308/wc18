import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ApiService } from "~/shared/api-service.service";

@Component({
  selector: "Fixture",
  moduleId: module.id,
  templateUrl: "./fixture.component.html",
  providers: [ApiService],
  styleUrls: ["./fixture.component.css"]
})
export class FixtureComponent implements OnInit {
  fixtures: Array<any>;
  loading: boolean = false;
  busy: boolean = true;

  constructor(private apiService: ApiService) {
    // Use the component constructor to inject providers.
  }

  ngOnInit(): void {
    // Init your component properties here.
    this.getFixtures();
  }

  getFixtures() {
    this.apiService.getFixtures().subscribe((res: any) => {
      this.fixtures = res.fixtures;
      this.loading = true;
      this.busy = false;
    });
  }

  getResult(val) {
    if (val == null) {
      return "-";
    } else {
      return val;
    }
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }
}
