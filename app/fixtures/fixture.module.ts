import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { FixtureRoutingModule } from "./fixture-routing.module";
import { FixtureComponent } from "./fixture.component";
import { MomentModule } from "angular2-moment";

@NgModule({
  imports: [
    NativeScriptCommonModule,
    FixtureRoutingModule,
    NativeScriptUIListViewModule,
    MomentModule
  ],
  declarations: [FixtureComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class FixtureModule {}
